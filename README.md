# FreeBSD sysctl MIB in HTML

Tool to save the [FreeBSD](https://www.freebsd.org) sysctl MIB in HTML.

**Getting Started**

```
% sudo pkg install libsysctlmibinfo2
% git clone https://gitlab.com/alfix/sysctl-mib-html.git
% cd sysctl-mib-html
% cc -I/usr/local/include sysctl-mib-html.c -L/usr/local/lib -lsysctlmibinfo2 -o sysctl-mib-html
% ./sysctl-mib-html -a
% cd MIB
% firefox index.html
```
FreeBSD provides thousands of sysctl objects,
building pages could take a while.


**Usage**

```
usage: sysctl-html-mib [-h | -V] [-d <dir>] -a | <name> ...

 -a		 Complete MIB, <name> subtree otherwise
 -d <dir>	 Save in <dir> (default ./MIB/)
 -h		 Display this help
 -V		 Add values
```
Note: \<name\> has to be a top level object, example *kern*, *vm*, *sys*,
and so on.

**Example**

```
% ./sysctl-mib-html -a
```
output: https://alfix.gitlab.io/sysctl-mib-html

