/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 *
 * To the extent possible under law, the author has dedicated all copyright and
 * related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */


#include <sys/param.h>
#include <sys/sysctl.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysctlmibinfo2.h>
#include <unistd.h>

#define IS_LEAF(node)	(node->children == NULL || SLIST_EMPTY(node->children))

char basedir[MAXPATHLEN];
bool aflag = false;
bool Vflag = false;

struct ctl_flag {
    unsigned int flag_bit;
    const char *flag_name;
    const char *flag_desc;
};

#define NUM_CTLFLAGS 21
struct ctl_flag ctl_flags[NUM_CTLFLAGS] = {
    { CTLFLAG_RD,       "RD",      "Can read the value" },
    { CTLFLAG_WR,       "WR",      "Can write the value" },
    { CTLFLAG_RW,       "RW",      "RD and WR" },
    { CTLFLAG_ANYBODY,  "ANYBODY", "All users can set this var" },
    { CTLFLAG_TUN,      "TUN",     "Default value is loaded from getenv()" },
    { CTLFLAG_RDTUN,    "RDTUN",   "RD and TUN" },
    { CTLFLAG_RWTUN,    "RWTUN",   "RW and TUN" },
    { CTLFLAG_STATS,    "STATS",   "Statistics, not a tuneable" },
    { CTLFLAG_NOFETCH,  "NOFETCH", "Don't fetch tunable from getenv()" },
    { CTLFLAG_CAPRD,    "CAPRD",   "Can read the value in Capability mode" },
    { CTLFLAG_CAPWR,    "CAPWR",   "Can write the value Capability mode" },
    { CTLFLAG_CAPRW,    "CAPRW",   "CAPRD and CAPWR" },
    { CTLFLAG_SECURE,   "SECURE",  "Can write the value only if securelevel<=0" },
    { CTLFLAG_PRISON,   "PRISON",  "Prisoned roots can fiddle" },
    { CTLFLAG_VNET,     "VNET",    "Prisons with vnet can fiddle" },
    { CTLFLAG_MPSAFE,   "MPSAFE",  "Multiprocessor safe" },
    { CTLFLAG_NEEDGIANT,"NEEDGIANT", "Requires Giant lock" },
    { CTLFLAG_DYN,      "DYN",     "Dynamic parameter/OID (can be freed)" },
    { CTLFLAG_DYING,    "DYING",   "Parameter is being removed (kernel debug)" },
    { CTLFLAG_DORMANT,  "DORMANT", "The parameter is not active yet (kernel debug)" },
    { CTLFLAG_SKIP,     "SKIP",    "Skip the value when listing" }
};

struct ctl_type {
    char *name;
    size_t size;
};

static const struct ctl_type ctl_types[CTLTYPE+1] = {
    { "ZEROUNUSED",       0                     },
    { "node",             0                     },
    { "integer",          sizeof(int)           },
    { "string",           0                     },
    { "int64_t",          sizeof(int64_t)       },
    { "opaque",           0                     },
    { "unsigned integer", sizeof(unsigned int)  },
    { "long integer",     sizeof(long int)      },
    { "unsigned long",    sizeof(unsigned long) },
    { "uint64_t",         sizeof(uint64_t)      },
    { "uint8_t",          sizeof(uint8_t)       },
    { "uint16_t",         sizeof(uint16_t)      },
    { "int8_t",           sizeof(int8_t)        },
    { "int16_t",          sizeof(int16_t)       },
    { "int32_t",          sizeof(int32_t)       },
    { "uint32_t",         sizeof(uint32_t)      }
};

void print_header(FILE *fp, char *title)
{
	fputs("<html>\n",fp);
	fputs("<head>\n",fp);
	fprintf(fp, "<title>%s</title>\n", title);
	fputs("<style>\n", fp);
	fputs("table, td, th { border: 1px solid #ddd; text-align: left; }\n", fp);
	fputs("table { border-collapse: collapse; }\n", fp);
	fputs("th, td { padding: 4px;}\n", fp);
	fputs("</style>\n", fp);
	fputs("</head>\n", fp);
	fputs("<body>\n", fp);
	fprintf(fp, "<h1>%s</h1>\n", title);
}

float get_IK_value(char* fmt, void *value, size_t value_size)
{
    int i, prec = 1, intvalue = *((int*)value);
    float base;
    
    if (fmt[2] != '\0')
	prec = fmt[2] - '0';
    base = 1.0;
    for (i = 0; i < prec; i++)
	base *= 10.0;

    return ((float)intvalue / base - 273.15);
}

void print_value(FILE *fp, struct sysctlmif_object *object)
{
    int i;
    void *value;
    size_t valuelen;

    if (object->type == CTLTYPE_NODE)
	return;
    if (object->type == CTLTYPE_OPAQUE)
	return;
    if (!(object->flags & CTLFLAG_RD))
	return;

    if (sysctl(object->id, object->idlevel, NULL, &valuelen, NULL, 0) != 0)
	return;
    valuelen += valuelen;
    if ((value = malloc(valuelen)) == NULL)
	return;
    if (sysctl(object->id, object->idlevel, value, &valuelen, NULL, 0) != 0) {
	free(value);
	return;
    }
    
    fputs("<tr>\n<td>Value</td>\n", fp);
    
    if (object->type == CTLTYPE_STRING) {
	if (((char*)value)[valuelen] != '\0')
	    ((char*)value)[valuelen] = '\0';
	fprintf(fp, "<td>%s</td>\n</tr>\n", (char *)value);
	free(value);
	return;
    }

    if (object->type == CTLTYPE_INT) {
	if (strncmp(object->fmt, "IK", 2) == 0)
	    ((char*)value)[valuelen] = '\0';
	fprintf(fp, "<td>%gC</td>\n</tr>\n",
	    get_IK_value(object->fmt, value, valuelen));
	free(value);
	return;
    }

    fputs("<td>\n", fp);
    for (i = 0; i < valuelen / ctl_types[object->type].size; i++) {
	if (i > 0)
	    fputs(" ", fp);

	switch (object->type) {
	case CTLTYPE_INT:
	    fprintf(fp, "%d", ((int*)value)[i]);
	    break;
	case CTLTYPE_LONG:  
	    fprintf(fp, "%ld", ((long*)value)[i]);    
	    break;
	case CTLTYPE_S8:    
	    fprintf(fp, "%d", ((int8_t*)value)[i]);  
	    break;
	case CTLTYPE_S16:   
	    fprintf(fp, "%d", ((int16_t*)value)[i]); 
	    break;
	case CTLTYPE_S32:   
	    fprintf(fp, "%d", ((int32_t*)value)[i]); 
	    break;
	case CTLTYPE_S64:   
	    fprintf(fp, "%ld", ((int64_t*)value)[i]); 
	    break;
	case CTLTYPE_UINT:  
	    fprintf(fp, "%u", ((u_int*)value)[i]);   
	    break;
	case CTLTYPE_ULONG: 
	    fprintf(fp, "%lu", ((u_long*)value)[i]);  
	    break;
	case CTLTYPE_U8:    
	    fprintf(fp, "%u", ((uint8_t*)value)[i]); 
	    break;
	case CTLTYPE_U16:   
	    fprintf(fp, "%u", ((uint16_t*)value)[i]);
	    break;
	case CTLTYPE_U32:   
	    fprintf(fp, "%u", ((uint32_t*)value)[i]);
	    break;
	case CTLTYPE_U64:   
	    fprintf(fp, "%lu", ((uint64_t*)value)[i]);
	    break;
	}
    }
    fputs("</td>\n</tr>\n", fp);
    free(value);
}

/* preorder visit */
void show_tree(struct sysctlmif_object *node, char* parentfile, int depth)
{
	struct sysctlmif_object *child;
	char name[MAXPATHLEN], filename[MAXPATHLEN], dir[MAXPATHLEN], *link;
	char parent[MAXPATHLEN];
	int i;
	FILE *fp;

	strcpy(name, node->name);
	if(name[strlen(name) -1] == '.')
	strcpy(name + strlen(name), "EMPTYNAME");
	for(i=0; i<strlen(name); i++){
		if(name[i] == '.')
			name[i] = '/';
		if(name[i] == '%')
			name[i] = 'X';
	}

	snprintf(dir, MAXPATHLEN, "%s/%s", basedir, name);
	mkdir(dir, 0700);
	snprintf(filename, MAXPATHLEN, "%s/%s/index.html", basedir, name);

	/* parent table */
	if((link = strrchr(name, '/')) == NULL)
		link = name;
	else
		link = link + 1;
	fp = fopen(parentfile, "a");	
	fputs("<tr>\n", fp);
	fprintf(fp, "<td><a href=./%s/index.html>%s</a></td>\n", link, node->name);
	fprintf(fp, "<td>%s\n</td>\n", node->desc);
	fputs("</tr>\n", fp);
	fclose(fp);

	/* object page */	
	fp = fopen(filename, "w");
	print_header(fp, node->name);

	fputs("<b><a href=\"", fp);
	for(i = 0; i < depth; i++)
		fputs("../", fp);
	fputs("index.html\">Home</a></b>\n", fp);
	fputs("<br /><br />\n", fp);
	if (depth > 1) {
		strcpy(parent, node->name);
		strrchr(parent, '.')[0] = '\0';
		fprintf(fp, "<b>Up: <a href=\"../index.html\">%s</a></b>\n", parent);
	}
	fputs("<br /><br />\n", fp);
	fputs("<table>\n", fp);
	fputs("<tr>\n", fp);
	fputs("<th>Property</th>\n", fp);
	fputs("<th>Value</th>\n", fp);
	fputs("</tr>\n", fp);

	fputs("<tr>\n", fp);
	fputs("<td>OID</td>\n", fp);
	fputs("<td>\n", fp);
	for (i = 0; i < node->idlevel; i++)
		fprintf(fp, "%x%c", node->id[i], i+1 < node->idlevel ? '.' : ' ');
	fputs("</td>\n", fp);
	fputs("</tr>\n", fp);

	fputs("<tr>\n<td>Description</td>\n", fp);
	fprintf(fp, "<td>%s</td>\n</tr>\n", node->desc);
	fputs("<tr><td>Label</td>", fp);
	fprintf(fp, "<td>%s</td>\n</tr>\n", node->label);
	fputs("<tr>\n<td>Type</td>\n", fp);
	fprintf(fp, "<td>%s</td>\n</tr>\n", ctl_types[node->type].name);
	fputs("<tr>\n<td>Format</td>\n", fp);
	fprintf(fp, "<td>%s</td>\n</tr>\n", node->fmt);
	fputs("<tr>\n<td>Flags</td>\n", fp);
	fputs("<td>\n", fp);
	fputs("<ul>\n", fp);
	for(i=0; i < NUM_CTLFLAGS; i++) {
	    if((node->flags & ctl_flags[i].flag_bit) == ctl_flags[i].flag_bit)
		fprintf(fp, "<li>%s, %s</li>\n",
		    ctl_flags[i].flag_name, ctl_flags[i].flag_desc);
	}
	fputs("</ul>\n", fp);
	fputs("</td>\n</tr>\n", fp);
	if(Vflag && IS_LEAF(node))
		print_value(fp, node);
	fputs("</table>\n", fp);

	if(!IS_LEAF(node)) {
		fputs("<br><br>Children:<br>\n", fp);
		fputs("<table>\n", fp);
		fputs("<tr>\n", fp);
		fputs("<th>Child</th>\n", fp);
		fputs("<th>Description</th>\n", fp);
		fputs("</tr>\n", fp);
		fclose(fp);
	
		SLIST_FOREACH(child, node->children, object_link)
			show_tree(child, filename, depth + 1);

		fp = fopen(filename, "a");
		fputs("</table>\n", fp);
	}
	
	fputs("</body>\n", fp);
	fputs("</html>\n", fp);
	fclose(fp);
}

void usage(void)
{

	printf("usage: sysctl-html-mib [-h | -V] [-d <dir>] -a | <name> ...\n");
}

int main(int argc, char **argv)
{
	struct sysctlmif_list *mib;
	struct sysctlmif_object *root;
	char filename[MAXPATHLEN];
	FILE *fp;
	int input;

	strcpy(basedir, "./MIB");

	while ((input = getopt(argc, argv, "ad:hV")) != -1) {
	switch (input) {
	case 'a':
		aflag = true;
		break;
	case 'd':
		strcpy(basedir, optarg);
		break;
	case 'h':
		usage();
		printf("\n");
		printf(" -a\t\t Complete MIB, <name> subtree otherwise\n");
		printf(" -d <dir>\t Save in <dir> (default ./MIB/)\n");
		printf(" -h\t\t Display this help\n");
		printf(" -V\t\t Add values\n");
		printf("\n");
		return 0;
	case 'V':
		Vflag = true;
		break;
	default:
		usage();
		return 1;
	}
	}
	argc -= optind;
	argv += optind;

	mkdir(basedir, 0700);
	sprintf(filename, "%s/index.html", basedir);
	fp = fopen(filename, "w");
	print_header(fp, "FreeBSD sysctl MIB");
	fputs("<br><br>\n", fp);
	fputs("<table>\n", fp);
	fputs("<tr><th>Tree</th>\n", fp);
	fputs("<th>Description</th></tr>\n", fp);
	fclose(fp);


	if (argc > 0) { /* <name> in input */
		argc = 0;
		while (argv[argc]) {
			if ((root = sysctlmif_treebyname(argv[argc])) != NULL) {
				show_tree(root, filename, 1);
				sysctlmif_freetree(root);
			} else
				printf("Bad name: \"%s\"\n", argv[argc]);
	    		argc++;
		}
    	}
	else if (aflag){
		if ((mib = sysctlmif_mib()) != NULL) {
			SLIST_FOREACH(root, mib, object_link)
	    			show_tree(root, filename, 1);
			sysctlmif_freemib(mib);
		}
    	}
    	else
    		usage();
	
	fp = fopen(filename, "a");
	fputs("</table>\n", fp);
	fputs("</body>\n", fp);
	fputs("</html>\n", fp);
	fclose(fp);

	return (0);
}
